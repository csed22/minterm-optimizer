#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define BACKSPACE 8
#define ENTER 13
#define ESC 27
#define COMMA 44
#define FFIVE 63
#define UP 72           // (H)
#define LEFT 75         // (K)
#define RIGHT 77        // (M)
#define DOWN 80         // (P)

#define BLACKBOX 219    // (█)
#define NWCORNER 218    // (┌)
#define NECORNER 191    // (┐)
#define SECORNER 217    // (┘)
#define SWCORNER 192    // (└)

#define UPT 193         // (┴)
#define DOWNT 194       // (┬)
#define RIGHTT 195      // (├)
#define LEFTT 193       // (┤)

#define APLUS 197       // (┼)

#define VBAR 179        // (│)
#define HBAR 196        // (─)

//9 Segments Constants.

#define BETWEENDIGITS 6
#define FROMMARGIN 20

// MAKE IT OPTIMIZED! (PURE HABD, 2NY 2ASEF ^_^ )

int menu();

void custom();

int main() {

    SetConsoleTitle("Logic circuit simplification (SOP)");

    while (1) {
        switch (menu()) {
            case 1  :
                break;
            case -1  :
                custom();
                break;
        }
    }
    return 0;
}

void beforeSelectedOption(int, int);

void afterSelectedOption(int, int);

void areYouSure();

int menu() {

    int position = 1;

    while (1) {

        system("cls"); // -> Windows, for UNIX -> system("clear");

        // http://ascii.co.uk/art/robot

        // https://www.asciiart.eu/computers/computers

        puts("");
        puts(" Sorry sir, but generate feature is currently unavailable.");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                                         ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("       \\         ..      \\");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                     _____               ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("        \\       /  `-.--.___ __.-.___");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                    |     |              ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("`-.      \\     /  #   `-._.-'    \\   `--.__");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                    | | | |              ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("`-.      \\     /  #   `-._.-'    \\   `--.__");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                    |_____|              ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("   `-.        /  ####    /   ###  \\        `.");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("              ____ ___|_|___ ____        ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("________     /  #### ############  |       _|           .'");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("             ()___)         ()___)       ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("            |\\ #### ##############  \\__.--' |    /    .'");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("             // /|           |\\ \\\\       ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("            | ####################  |       |   /   .'");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("            // / |           | \\ \\\\      ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("            | #### ###############  |       |  /");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("           (___) |___________| (___)     ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("            | #### ###############  |      /|      ----");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("           (___)   (_______)   (___)     ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("          . | #### ###############  |    .'<    ____");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("           (___)     (___)     (___)     ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("        .'  | ####################  | _.'-'\\|");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("           (___)      |_|      (___)     ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("      .'    |   ##################  |       |");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("           (___)  ___/___\\___   | |      ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("             `.   ################  |       |");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("            | |  |           |  | |      ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("               `.    ############   |       | ----");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("            | |  |___________| /___\\     ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("              ___`.     #####     _..____.-'     .");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("           /___\\  |||     ||| //   \\\\    ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("             |`-._ `-._       _.-'    \\\\\\         `.");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("          //   \\\\ |||     ||| \\\\   //    ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("        .' .. . `-._  `-._        ___.---'|   \\   \\");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("          \\\\   // |||     |||  \\\\ //     ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("      .' .. . .. .  `-._  `-.__.-'        |    \\   \\");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("           \\\\ // ()__)   (__()           ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("     |`-. . ..  . .. .  `-._|             |     \\   \\");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                 ///       \\\\\\           ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("     |   `-._ . ..  . ..   .'            _|");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                ///         \\\\\\          ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("      `-._   `-._ . ..   .' |      __.--'");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("              _///___     ___\\\\\\_        ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("          `-._   `-._  .' .'|__.--'");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("             |_______|   |_______|       ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("              `-._   `' .'");
        for (int i = 0; i < 12; i++) { printf(" "); }
        printf("                                         ");
        for (int i = 0; i < 35; i++) { printf(" "); }
        puts("                  `-._.'");

        // Many spaces are added in the drawings or in text because of "\" being written as "\\", so spaces add to make the end of text or picture in the same column.

        puts("");
        // https://www.messletters.com/en/big-text/

        for (int i = 0; i < 7; i++) { printf(" "); }
        beforeSelectedOption(1, position);
        printf("  _____                                       _          ");
        afterSelectedOption(1, position);
        for (int i = 0; i < 25; i++) { printf(" "); }
        beforeSelectedOption(-1, position);
        puts("  _____                 _                       ");
        afterSelectedOption(-1, position);
        for (int i = 0; i < 7; i++) { printf(" "); }
        beforeSelectedOption(1, position);
        printf(" / ____|                                     | |         ");
        afterSelectedOption(1, position);
        for (int i = 0; i < 25; i++) { printf(" "); }
        beforeSelectedOption(-1, position);
        puts(" / ____|               | |                      ");
        afterSelectedOption(-1, position);
        for (int i = 0; i < 7; i++) { printf(" "); }
        beforeSelectedOption(1, position);
        printf("| |  __    ___   _ __     ___   _ __   __ _  | |_    ___ ");
        afterSelectedOption(1, position);
        for (int i = 0; i < 25; i++) { printf(" "); }
        beforeSelectedOption(-1, position);
        puts("| |       _   _   ___  | |_    ___    _ __ ___  ");
        afterSelectedOption(-1, position);
        for (int i = 0; i < 7; i++) { printf(" "); }
        beforeSelectedOption(1, position);
        printf("| | |_ |  / _ \\ | '_ \\   / _ \\ | '__| / _` | | __|  / _ \\");
        afterSelectedOption(1, position);
        for (int i = 0; i < 25; i++) { printf(" "); }
        beforeSelectedOption(-1, position);
        puts("| |      | | | | / __| | __|  / _ \\  | '_ ` _ \\ ");
        afterSelectedOption(-1, position);
        for (int i = 0; i < 7; i++) { printf(" "); }
        beforeSelectedOption(1, position);
        printf("| |__| | |  __/ | | | | |  __/ | |   | (_| | | |_  |  __/");
        afterSelectedOption(1, position);
        for (int i = 0; i < 25; i++) { printf(" "); }
        beforeSelectedOption(-1, position);
        puts("| |____  | |_| | \\__ \\ | |_  | (_) | | | | | | |");
        afterSelectedOption(-1, position);
        for (int i = 0; i < 7; i++) { printf(" "); }
        beforeSelectedOption(1, position);
        printf(" \\_____|  \\___| |_| |_|  \\___| |_|    \\__,_|  \\__|  \\___|");
        afterSelectedOption(1, position);
        for (int i = 0; i < 25; i++) { printf(" "); }
        beforeSelectedOption(-1, position);
        puts(" \\_____|  \\__,_| |___/  \\__|  \\___/  |_| |_| |_|");
        afterSelectedOption(-1, position);
        for (int i = 0; i < 7; i++) { printf(" "); }
        beforeSelectedOption(1, position);
        printf("                                                         ");
        afterSelectedOption(1, position);
        for (int i = 0; i < 25; i++) { printf(" "); }
        beforeSelectedOption(-1, position);
        puts("                                                ");
        afterSelectedOption(-1, position);

        int keyPressed;

        while (1) {                      // To stop crazy refreshes if he didn't press an acceptable key.
            keyPressed = (int) getch();
            if (keyPressed == RIGHT || keyPressed == LEFT || keyPressed == ENTER || keyPressed == ESC)
                break;
        }

        if (keyPressed == RIGHT || keyPressed == LEFT)
            position = -1 * position;                   // Just switch it.
        else if (keyPressed == ENTER)
            return position;                           // Back to main.
        else if (keyPressed == ESC)
            areYouSure();
    }
}

void setColorAndBackground(int ForgC, int BackC) {
    WORD wColor = ((BackC & 0x0F) << 4) + (ForgC &
                                           0x0F);                                   // Takes the first 4 digits of the number as they are only from 0 to 15
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),
                            wColor);                       // Just like system("COLOR [Back][Fore]"); but more efficient (You can color a line or a part of it)!
}   // https://www.codewithc.com/highlight-change-text-background-color-in-codeblocks-console-window/

void beforeSelectedOption(int realPosition,
                          int arrowPosition) { // Highlights it with a normal WHITE color, and changes the font color to LIGHT BLUE.
    if (realPosition == arrowPosition)
        setColorAndBackground(9,
                              7);                           // The table of colors is in this report (My previous chess project) : https://drive.google.com/open?id=1W6GkFnbi9CTI1DXemjZOljdlx71FCLfu
}   // https://youtu.be/StEBDXY2WBk

void afterSelectedOption(int realPosition,
                         int arrowPosition) { // Return the text after it to the default (WHITE font color, BLACK back color).
    if (realPosition == arrowPosition)
        setColorAndBackground(7, 0);
}   // https://youtu.be/StEBDXY2WBk


void areYouSure() {
    // If Yes exit(0) ,If No just continue ( does nothing -> back to previous menu ).
    int keyPressed = 0, position = 1;

    while (keyPressed != ENTER) {

        system("cls");
        printf("\n\n\n");              // Constants defined from extended ASCII table values, better readability ( Example : NECORNER = North, East, Corner ).

        printf("\t%c", NWCORNER);
        for (int i = 0; i < 40; i++) { printf("%c", HBAR); }
        printf("%c\n", NECORNER);
        printf("\t%c", VBAR);
        printf(" Are you sure you want to exit ?        ");
        printf("%c\n", VBAR);
        printf("\t%c", VBAR);
        for (int i = 0; i < 25; i++) { printf(" "); }
        printf("%c%c%c%c%c%c%c %c%c%c%c%c%c %c\n", NWCORNER, HBAR, HBAR, HBAR, HBAR, HBAR, NECORNER, NWCORNER, HBAR,
               HBAR, HBAR, HBAR, NECORNER, VBAR);
        printf("\t%c", VBAR);
        for (int i = 0; i < 25; i++) { printf(" "); }
        printf("%c ", VBAR);

        beforeSelectedOption(1, position);
        printf("Yes");
        afterSelectedOption(1, position);
        printf(" %c %c ", VBAR, VBAR);
        beforeSelectedOption(2, position);
        printf("No");
        afterSelectedOption(2, position);
        printf(" %c %c\n", VBAR, VBAR);

        printf("\t%c", VBAR);
        for (int i = 0; i < 25; i++) { printf(" "); }
        printf("%c%c%c%c%c%c%c %c%c%c%c%c%c %c\n", SWCORNER, HBAR, HBAR, HBAR, HBAR, HBAR, SECORNER, SWCORNER, HBAR,
               HBAR, HBAR, HBAR, SECORNER, VBAR);
        printf("\t%c", SWCORNER);
        for (int i = 0; i < 40; i++) { printf("%c", HBAR); }
        printf("%c\n", SECORNER);

        while (1) {                      // To stop crazy refreshes if he didn't press an acceptable key.
            keyPressed = (int) getch();
            if (keyPressed == RIGHT || keyPressed == LEFT || keyPressed == ENTER || keyPressed == ESC)
                break;
        }

        if (keyPressed == RIGHT || keyPressed == LEFT)
            if (position == 1)
                position = 2;
            else
                position = 1;

        if (keyPressed == ESC)
            break;                      // ESC from the areYouSure menu -> back to previous menu.
    }

    if ((position == 1) && (keyPressed == ENTER))
        exit(0);                    // Terminate the program.
}

int scanNineSegmentNumber(int *);

void letsGetStarted(int);

void custom() {

    int x = 0;
    int num[5] = {0, 0, 0, 0, 0};
    while (x == 0) {
        x = scanNineSegmentNumber(num);
    }

    letsGetStarted(x);

}

void rePrintScanScreen(int, int *);

int validDigit(int);

void convertToBinary5Digits(int, int *);

void nineSegments(int, int *);

int scanNineSegmentNumber(int num[]) {

    system("cls");

    int i = 0; //digit indicator.

    int digit;
    int x = 0;

    rePrintScanScreen(x, num);

    while (i < 2) {


        while (1) {                      // To stop crazy refreshes if he didn't press an acceptable key.
            digit = (int) getch();
            if (digit == UP || digit == DOWN || digit == ENTER || digit == ESC || digit == BACKSPACE ||
                validDigit(digit))
                break;
        }

        if (digit == UP) {
            x++;
            if (x > 19) { x = 0; }
        } else if (digit == DOWN) {
            x--;
            if (x < 0) { x = 19; }
        } else if (digit == ESC) {
            areYouSure();
            rePrintScanScreen(x, num);
        } else if (digit == ENTER)
            break;
        else if (digit == BACKSPACE) {
            printf("\r"); //Rewind To Its Beginning ( https://stackoverflow.com/questions/1508490/erase-the-current-printed-console-line ) .
            printf("                                                                    ");
            i = 0;
            x = 0;
        }

        if (validDigit(digit) && i < 2) {

            digit = digit - 48;

            if (x == 1 && i) {
                i++;
                x = x + 9 + digit;
                printf("%d", digit);
            } else if (x == 0 && i) {
                i++;
                x = digit;
                printf("%d", digit);
            } else if (i) {
                //Nothing.
            } else {
                i++;
                x = digit;
                printf("%d", digit);
            }

        }


        rePrintScanScreen(x, num);

        if (digit != ENTER && i == 2) {
            while (1) {
                while (1) {                      // To stop crazy refreshes if he didn't press an acceptable key.
                    digit = (int) getch();
                    if (digit == ENTER || digit == ESC || digit == BACKSPACE)
                        break;
                }

                if (digit == ESC) {
                    areYouSure();
                    i = 0;
                    x = 0;
                    rePrintScanScreen(x, num);
                    break;
                } else if (digit == ENTER)
                    break;
                else if (digit == BACKSPACE) {
                    i = 0;
                    x = 0;
                    rePrintScanScreen(x, num);
                    break;
                }
            }
        }
    }

    return x;
}

void rePrintScanScreen(int x, int num[]) {

    system("cls");
    puts("");
    convertToBinary5Digits(x, num);
    nineSegments(x, num);

    puts("How many variables ?");
    puts("                      (You can use arrows or number pad to input) : ");
    printf("                                                                    ");

}

int validDigit(int x) {
    if (x > 47 && x < 58)
        return 1;
    return 0;
}

void convertToBinary5Digits(int x, int num[5]) {
    for (int i = 0; i < 5; i++) {
        num[i] = x % 2;
        x = x / 2;
    }
}

int not(int);

void print2VnoHeadnoTail(int, int, int);

void printH(int, int, int, int, int, int, int);

void nineSegments(int x, int num[]) {

    int A = num[4];
    int B = num[3];
    int C = num[2];
    int D = num[1];
    int E = num[0];

    int a = A + not(C) * not(E) + not(B) * D + C * E + B * not(D);
    int b = not(C) * E + not(C) * D + B * not(D) + B * not(E) + not(A) * not(D) * not(E) + not(B) * D * E;
    int c = E + A + not(B) * not(D) + C * D + B * not(C);
    int d = not(C) * not(E) + not(B) * D * not(E) + C * not(D) * E + B * not(D) * not(E) + B * C * E +
            not(A) * not(B) * not(C) * D;
    int e = not(C) * not(E) + not(B) * D * not(E) + B * not(D) * not(E);
    int f = A * D + not(B) * not(D) * not(E) + not(B) * C * not(D) + not(B) * C * not(E) + B * not(C) * not(D) +
            B * not(C) * not(E) + B * C * D;
    int g = C * not(D) + C * not(E) + B * not(D) + B * C + A * not(E) + not(B) * not(C) * D;
    int h = A + B * D + B * C;
    //int i = x>9? 1:0;

    printH(+1, a, b, c, e, f, h);           // No heads
    print2VnoHeadnoTail(f, b, h);
    printH(0, g, b, c, e, f, h);
    print2VnoHeadnoTail(e, c, h);
    printH(-1, d, b, c, e, f, h);           // No tails

}

int not(int x) {
    if (x) {
        return 0;
    } else {
        return 1;
    }
}

void printH(int checker, int x, int b, int c, int e, int f, int h) {    //Ultra Complicated.

    if (checker != +1) {

        for (int i = 0; i < FROMMARGIN; i++)
            printf(" ");

        if (h) {
            printf("%c", SWCORNER);
            printf("%c%c", HBAR, HBAR);
            printf("%c", SECORNER);
        } else { for (int i = 0; i < 4; i++) { printf(" "); }}

        for (int i = 0; i < BETWEENDIGITS; i++)
            printf(" ");

        if ((checker == 0 && f) || (checker == -1 && e)) {
            printf("%c", SWCORNER);
            printf("%c%c", HBAR, HBAR);
            printf("%c", SECORNER);
        } else
            for (int i = 0; i < 4; i++) { printf(" "); }
    }                  //└──┘ Left ( f for 0 case & e for -1 case )

    if (checker == +1) { for (int i = 0; i < BETWEENDIGITS + FROMMARGIN + 8; i++) { printf(" "); }} //Leave it empty


    if (x) {
        printf("%c", NWCORNER);
        for (int i = 0; i < 12; i++) { printf("%c", HBAR); }
        printf("%c", NECORNER);
    }   //Up of Box
    else { for (int i = 0; i < 14; i++) { printf(" "); }}


    if (checker == +1) { printf("\n"); } //Because it is empty in the left side too

    if (checker != +1) {
        if ((checker == 0 && b) || (checker == -1 && c)) {
            printf("%c", SWCORNER);
            printf("%c%c", HBAR, HBAR);
            printf("%c\n", SECORNER);
        } else
            printf("\n");
    }                //└──┘ Right ( b for 0 case & c for -1 case )

    //End of first line.

    for (int i = 0; i < BETWEENDIGITS + FROMMARGIN + 4; i++) { printf(" "); }

    if (x) {
        for (int i = 0; i < 4; i++) { printf(" "); }
        printf("%c", VBAR);
        for (int i = 0; i < 12; i++) { printf("%c", BLACKBOX); }
        printf("%c\n", VBAR);
    }           //Box Body
    else {
        for (int i = 0; i < 14; i++) { printf(" "); }
        printf("\n");
    }

    //End of second line.

    if (checker != -1) {

        for (int i = 0; i < FROMMARGIN; i++)
            printf(" ");

        if (h) {
            printf("%c", NWCORNER);
            printf("%c%c", HBAR, HBAR);
            printf("%c", NECORNER);
        } else { for (int i = 0; i < 4; i++) { printf(" "); }}

        for (int i = 0; i < BETWEENDIGITS; i++)
            printf(" ");

        if ((checker == 0 && e) || (checker == +1 && f)) {
            printf("%c", NWCORNER);
            printf("%c%c", HBAR, HBAR);
            printf("%c", NECORNER);
        } else
            for (int i = 0; i < 4; i++) { printf(" "); }
    }                  //┌──┐ Left ( e for 0 case & f for +1 case )

    if (checker == -1) { for (int i = 0; i < BETWEENDIGITS + FROMMARGIN + 8; i++) { printf(" "); }} //Leave it empty


    if (x) {
        printf("%c", SWCORNER);
        for (int i = 0; i < 12; i++) { printf("%c", HBAR); }
        printf("%c", SECORNER);
    }   //Bottom of Box
    else { for (int i = 0; i < 14; i++) { printf(" "); }}


    if (checker == -1) { printf("\n"); } //Because it is empty in the left side too

    if (checker != -1) {

        if ((checker == 0 && c) || (checker == +1 && b)) {
            printf("%c", NWCORNER);
            printf("%c%c", HBAR, HBAR);
            printf("%c\n", NECORNER);
        } else
            printf("\n");
    }                //┌──┐ Right ( c for 0 case & b for +1 case )

    //End of third line.

}

void print2VnoHeadnoTail(int V1, int V2, int h) {

    for (int i = 0; i < 5; i++) {

        for (int i = 0; i < FROMMARGIN; i++)
            printf(" ");

        if (h) { printf("%c%c%c%c", VBAR, BLACKBOX, BLACKBOX, VBAR); }
        else { for (int i = 0; i < 4; i++) { printf(" "); }}

        for (int i = 0; i < BETWEENDIGITS; i++)
            printf(" ");

        if (V1) { printf("%c%c%c%c", VBAR, BLACKBOX, BLACKBOX, VBAR); }
        else { for (int i = 0; i < 4; i++) { printf(" "); }}

        for (int i = 0; i < 14; i++)
            printf(" ");

        if (V2) { printf("%c%c%c%c\n", VBAR, BLACKBOX, BLACKBOX, VBAR); }
        else { printf("\n"); }
    }
}

int powerIt(int, int);

int readTerms(char *, int, int);

struct term {
    int value;
    char taken;
    struct term *next;
};

void freeRow(struct term *);

struct term *buildTabularMethod(char *, int, int);

// Global printing variables :

int longestList = 0;        // After printing all to decide where to put the cursor back
int numberOfLevels = 0;     // To calculate if it can be displayed horizontally or just leave it vertical.

void buildpetricksMethod(char *, int, int *, int, int);

void letsGetStarted(int numOfVariables) {

    char table[powerIt(2, numOfVariables)];
    char dtable[powerIt(2, numOfVariables)];

    for (int i = 0; i < powerIt(2, numOfVariables); i++) {
        table[i] = 0;
        dtable[i] = 0;
    }

    int mintermsSuccess = 0;
    int dontcaresSuccess = 0;

    while (1) {

        longestList = 0;
        numberOfLevels = numOfVariables +
                         1;    // Is set to be the right value at the end of the recursive function applyTabularMethod

        system("cls");

        puts("Please, enter the minterms :    ( separate by a comma \",\" & Enter when you finish & F5 if you stuck & repetitions and out of range values are ignored )\n");
        printf("                             [ ");

        if (mintermsSuccess == 0) {     // First Time, Read
            mintermsSuccess = readTerms(table, powerIt(2, numOfVariables), 1);     // 1 is the indicator
            continue;
        } else if (mintermsSuccess == 3) {     // Backspace, Print ones then Read
            for (int i = 0; i < powerIt(2, numOfVariables); i++)
                if (table[i] == 1)
                    printf("%d, ", i);
            mintermsSuccess = readTerms(table, powerIt(2, numOfVariables), 1);
            continue;

        } else if (mintermsSuccess == 2) {     // Failed, Redo from start and Reset table
            for (int i = 0; i < powerIt(2, numOfVariables); i++)
                table[i] = 0;
            mintermsSuccess = 0;
            continue;
        }

        if (mintermsSuccess == 1) {         // Success, Print it, Close bracket
            for (int i = 0; i < powerIt(2, numOfVariables); i++)
                if (table[i] == 1)
                    printf("%d, ", i);
            printf(" ]\n\n");
        }

        puts("Please, enter the don't cares : ( Note : you can't overwrite the minterms! )\n");
        printf("                             [ ");

        if (dontcaresSuccess == 0) {     // First Time, Read
            dontcaresSuccess = readTerms(dtable, powerIt(2, numOfVariables), 2);     // 2 is the indicator
            continue;
        } else if (dontcaresSuccess == 4) {     // Backspace, Print ones then Read
            for (int i = 0; i < powerIt(2, numOfVariables); i++) {
                if (dtable[i] && table[i] == 1)
                    dtable[i] = 0;
                else if (dtable[i])
                    printf("%d, ", i);
            }
            dontcaresSuccess = readTerms(dtable, powerIt(2, numOfVariables), 2);
            continue;

        } else if (dontcaresSuccess == 3) {     // Failed, Redo from start and Reset dtable
            for (int i = 0; i < powerIt(2, numOfVariables); i++)
                if (dtable[i] == 2)
                    dtable[i] = 0;
            dontcaresSuccess = 0;
            continue;
        }

        if (dontcaresSuccess == 2) {         // Success, Print it, Close bracket
            for (int i = 0; i < powerIt(2, numOfVariables); i++) {
                if (dtable[i] && table[i] == 1)
                    dtable[i] = 0;
                else if (dtable[i])
                    printf("%d, ", i);
            }
            printf(" ]\n\n\n");
        }

        printf("Press any key to continue. ( F5 to input from start )");
        int changeYourMind = getch();

        if (changeYourMind == FFIVE || changeYourMind == ESC) {
            dontcaresSuccess = 3;
            mintermsSuccess = 2;
            if (changeYourMind == ESC)
                areYouSure();
        } else if (changeYourMind) {      // Because some special keys such as F5 gives two signals 0 first then 63. ( Keep that in mind )

            for (int i = 0; i < powerIt(2, numOfVariables); i++)
                if (table[i] == 0 && dtable[i])
                    table[i] = dtable[i];

            printf("\rTabular method time :                                \n");

            struct term *primeImplicants = buildTabularMethod(table, powerIt(2, numOfVariables), numOfVariables + 1);

            int i = 0;
            int n = 0;
            struct term *ptr = primeImplicants;

            while (ptr != NULL) {
                ptr = ptr->next;
                n++;
            }

            ptr = primeImplicants;

            int primeImplicantsArr[n];
            while (ptr != NULL) {
                primeImplicantsArr[i] = ptr->value;
                ptr = ptr->next;
                i++;
            }

            freeRow(primeImplicants);

            COORD newPosition = {0, 12 + longestList};
            SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), newPosition);

            for (int i = 0; i < n; i++) {
                if (n < 10)
                    printf("\t\t\t     arrayOfPrimeImplicants[%d] = ", i);
                else if (n < 100)
                    printf("\t\t\t     arrayOfPrimeImplicants[%.2d] = ", i);
                else if (n < 1000)
                    printf("\t\t\t     arrayOfPrimeImplicants[%.3d] = ", i);
                else if (n < 10000)
                    printf("\t\t\t     arrayOfPrimeImplicants[%.4d] = ", i);
                else if (n < 100000)
                    printf("\t\t\t     arrayOfPrimeImplicants[%.5d] = ", i);
                else if (n < 1000000)
                    printf("\t\t\t     arrayOfPrimeImplicants[%.6d] = ",
                           i); // More than 262144 is impossible because the cap is 19 variables
                int triArr[numOfVariables];
                convertToTrinry(primeImplicantsArr[i], triArr, numOfVariables);
                for (int j = numOfVariables - 1; j >= 0; j--) {
                    if (triArr[j] == 2)
                        printf("-");
                    else
                        printf("%d", triArr[j]);
                }
                printf("\n");
            }

            puts("\n");

            buildpetricksMethod(table, powerIt(2, numOfVariables), primeImplicantsArr, n, numOfVariables);

            /*
            REMOVE IT WHEN YOU FINISH
            */

            dontcaresSuccess = 3;
            mintermsSuccess = 2;
            getch();

        }
    }

}

int powerIt(int x, int y) {
    int result = 1;

    if (y) { result = x; }

    for (int i = 1; i < y; i++)
        result *= x;

    return result;
}

int readTerms(char table[], int n, int indicator) {

    int keyPressed = 0;
    int prevKeyPressed;
    int commaIsAllowed = 0;
    int numValue = 0;

    while (keyPressed != ENTER) {

        while (1) {                      // To stop crazy refreshes if he didn't press an acceptable key.
            keyPressed = (int) getch();
            if (keyPressed == ENTER || keyPressed == ESC || keyPressed == FFIVE || keyPressed == BACKSPACE ||
                validDigit(keyPressed))
                break;
            else if (keyPressed == COMMA && commaIsAllowed) {
                commaIsAllowed = 0;
                break;
            }
        }

        if (keyPressed == ESC) {
            areYouSure();
            return 2 + indicator;
        } else if (keyPressed == FFIVE)
            return 1 + indicator;
        else if (keyPressed == COMMA) {
            printf(", ");
            if (numValue > 0 && numValue < n)
                table[numValue] = indicator;
            if (prevKeyPressed == 48 & numValue == 0)
                table[0] = indicator;
            numValue = 0;
            commaIsAllowed = 0;
        } else if (keyPressed == ENTER) {
            if (numValue > 0 && numValue < n)
                table[numValue] = indicator;
            if (prevKeyPressed == 48 & numValue == 0)
                table[0] = indicator;
            break;
        } else if (keyPressed == BACKSPACE) {
            if (numValue > 0 && numValue < n)
                table[numValue] = 0;
            if (prevKeyPressed == 48 & numValue == 0)
                table[0] = indicator;
            return 2 + indicator;
        }

        if (validDigit(keyPressed)) {
            numValue = (keyPressed - 48) + (10 * numValue);
            printf("%d", keyPressed - 48);
            commaIsAllowed++;
        }

        prevKeyPressed = keyPressed;

    }

    return indicator;
}

void freeRow(struct term *start) {
    struct term *ptr = start;
    struct term *tmp;
    while (ptr != NULL) {
        tmp = ptr->next;
        free(ptr);
        ptr = tmp;
    }
}

int decBinTri(int);

int countOnes(int *, int);

void convertToTrinry(int, int *, int);

struct term *createTerm(int);

struct term *append(struct term *, struct term *);

void printTabular(struct term *baseColumn[], int, int);

struct term *applyTabularMethod(struct term *, struct term *baseColumn[], int, int);

struct term *buildTabularMethod(char table[], int size, int height) {

    struct term *baseColumn[height];
    for (int i = 0; i < height; i++)
        baseColumn[i] = NULL;

    int onesArray[height - 1];
    for (int i = 0; i < size; i++) {
        if (table[i]) {
            convertToTrinry(decBinTri(i), onesArray, height - 1);
            int n = countOnes(onesArray, height - 1);
            if (baseColumn[n] == NULL) {
                baseColumn[n] = createTerm(
                        decBinTri(i));       // Create a term and store it initially in the array of pointers
            } else {
                struct term *end = baseColumn[n];
                while (end->next != NULL) {
                    end = end->next;
                }
                struct term *newPtr = createTerm(decBinTri(i));
                end = append(end, newPtr);                      // Add anther term at the end of the list
            }
        }
    }

    struct term *primeImplicants = NULL;    // Don't forget to create the first element

    primeImplicants = applyTabularMethod(primeImplicants, baseColumn, height - 1, height - 1);
    printTabular(baseColumn, height, height - 1);

    for (int i = 0; i < height; i++)
        if (baseColumn[i] != NULL)
            freeRow(baseColumn[i]);

    return primeImplicants;

}

int decBinTri(int n) {   //*10 -> BinaryNumber With Decimal Values, *3 -> ,, With Trinary Values
    if (n)
        return (n % 2 + 3 * decBinTri(n / 2));
    return 0;
}

int countOnes(int arr[], int x) {
    int counter = 0;
    for (int i = 0; i < x; i++)
        if (arr[i])
            counter++;
    return counter;
}

void convertToTrinry(int x, int num[], int n) { //digit index = indicates the weight
    for (int i = 0; i < n; i++) {
        num[i] = x % 3;
        x = x / 3;
    }
}

struct term *createTerm(int term) {
    struct term *ptr;
    ptr = (struct term *) malloc(sizeof(struct term));
    ptr->value = term;
    ptr->taken = 0;
    ptr->next = NULL;
    return ptr;
}

struct term *append(struct term *end, struct term *newTermptr) {
    end->next = newTermptr;
    return (end->next);
}

void printTabular(struct term *baseColumn[], int height, int n) {

    if (((2 * (numberOfLevels - 1) + (n + 1) * numberOfLevels)) > 157) {
        for (int i = 0; i < height; i++) {
            struct term *end = baseColumn[i];
            while (end != NULL) {
                int onesArray[n];
                convertToTrinry(end->value, onesArray, n);
                printf("\t\t\t     ");
                for (int j = n - 1; j >= 0; j--) {
                    if (onesArray[j] == 2)
                        printf("-");
                    else
                        printf("%d", onesArray[j]);
                }
                if (end->taken == 0)
                    printf(" *");
                printf("\n");
                end = end->next;
            }
            if (i < height - 1) {
                printf("\t\t\t     ");
                for (int k = 0; k < n; k++)
                    printf("%c", HBAR);
                printf("\n");
            }
        }
        puts("\n\n");

    } else {

        COORD newPosition = {0, 12};
        SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), newPosition);

        int indentation = n + 1 - height;   // Number of previous levels
        int currentLength = 0;

        for (int i = 0; i < height; i++) {
            struct term *end = baseColumn[i];
            while (end != NULL) {
                int onesArray[n];
                convertToTrinry(end->value, onesArray, n);
                for (int k = 0; k <= (indentation * (n + 3)); k++)
                    printf(" ");
                for (int j = n - 1; j >= 0; j--) {
                    if (onesArray[j] == 2)
                        printf("-");
                    else
                        printf("%d", onesArray[j]);
                }
                if (end->taken == 0)
                    printf(" *");
                printf("\n");
                currentLength++;
                end = end->next;
            }
            if (i < height - 1) {
                for (int k = 0; k <= (indentation * (n + 3)); k++)
                    printf(" ");
                for (int k = 0; k < n; k++)
                    printf("%c", HBAR);
                printf("\n");
                currentLength++;

            }
        }

        puts("\n\n");
        currentLength++;
        if (currentLength > longestList)
            longestList = currentLength;

    }
}

int checkTheDifference(struct term *, struct term *, int);

int sumTrinaryArr(int *, int);

struct term *applyTabularMethod(struct term *primeImplicants, struct term *baseColumn[], int height, int n) {

    int didSomeThing;
    didSomeThing = 0;

    struct term *nextColumn[height];
    for (int i = 0; i < height; i++)
        nextColumn[i] = NULL;

    for (int i = 0; i < height; i++) {
        struct term *listPtr = baseColumn[i];
        struct term *nextListPtr = baseColumn[i + 1];

        while (listPtr != NULL) {

            while (nextListPtr != NULL) {

                int differenceIndex = checkTheDifference(listPtr, nextListPtr, n);

                if (differenceIndex > -1) {
                    //if yes add it, if it already exists ignore it but check it's taken, if no elements create.
                    didSomeThing++;
                    int newNum[n];
                    convertToTrinry(listPtr->value, newNum, n);

                    listPtr->taken = 1;
                    nextListPtr->taken = 1;
                    newNum[differenceIndex] = 2;

                    struct term *newPtr = createTerm(sumTrinaryArr(newNum, n));

                    if (nextColumn[i] == NULL) {
                        nextColumn[i] = newPtr;                         // Create a term and store it initially in the array of pointers
                    } else {
                        struct term *end = nextColumn[i];
                        int alreadyHere = 0;
                        while (end->next != NULL) {
                            if (newPtr->value == end->value)
                                alreadyHere++;
                            end = end->next;
                        }
                        if (newPtr->value == end->value)
                            alreadyHere++;
                        if (alreadyHere == 0)
                            end = append(end, newPtr);              // Add anther term at the end of the list
                        else
                            free(newPtr);
                    }

                }
                nextListPtr = nextListPtr->next;
            }
            listPtr = listPtr->next;
            nextListPtr = baseColumn[i + 1];
        }

    }

    // Search for non-taken elements to add to PI list

    for (int i = 0; i < height + 1; i++) {
        struct term *listPtr = baseColumn[i];

        while (listPtr != NULL) {

            if (listPtr->taken == 0) {
                struct term *newPtr = createTerm(listPtr->value);

                if (primeImplicants == NULL) {
                    primeImplicants = newPtr;                       // Create a term and store it initially in the list
                } else {
                    struct term *end = primeImplicants;
                    while (end->next != NULL) {
                        end = end->next;
                    }
                    end = append(end, newPtr);                  // Add anther term at the end of the list
                }
            }
            listPtr = listPtr->next;
        }
    }

    if (height && didSomeThing) {
        primeImplicants = applyTabularMethod(primeImplicants, nextColumn, height - 1, n);
        printTabular(nextColumn, height, n);
    }

    if (didSomeThing == 0)
        numberOfLevels = numberOfLevels - height;

    for (int i = 0; i < height; i++)
        if (nextColumn[i] != NULL)
            freeRow(nextColumn[i]);

    return primeImplicants;
}

int checkTheDifference(struct term *listPtr, struct term *nextListPtr, int n) {

    int upperNum[n];
    int lowerNum[n];

    convertToTrinry(listPtr->value, upperNum, n);
    convertToTrinry(nextListPtr->value, lowerNum, n);

    int counter = 0;
    int index = 0;

    for (int i = 0; i < n; i++) {
        if (upperNum[i] == 0 && lowerNum[i] == 1) {
            counter++;
            index = i;
        } else if (upperNum[i] == lowerNum[i]) {
        } else
            counter++;
    }

    if (counter == 1)
        return index;

    return -1; // More than one difference

}

int sumTrinaryArr(int Num[], int n) {
    int sum = 0;
    for (int i = 0; i < n; i++)
        sum += Num[i] * powerIt(3, i);
    return sum;
}

struct primeImp {
    int n;
    int pI;
    int *twosIndex;     //[n], don't forget to free them ...
    int *covers;        //[powerIt(2,n)], don't forget to free them ...
};

struct minTerm {
    int indecator;
    int value;
    int stillNeeded;
    int max;
    int *coveredBy;     //[max], don't forget to free them ...
};

void creatElementsInsidePrimeImp(struct primeImp *, int);

int countTwos(int *, int);

void creatElementsInsideMinTerm(struct minTerm *, int);

void applyPetrickMethod(struct minTerm *, int counter, struct term *, int, int);

void printThePI(struct primeImp *current) {

    printf("\nThe PI (%d) :", current->pI);
    printf("\nn = %d\n\n", current->n);
    for (int i = 0; i < current->n; i++)
        printf("twosIndex[%d] = %d\n", i, current->twosIndex[i]);

    for (int i = 0; i < powerIt(2, current->n); i++)
        printf("covers[%d] = %d\n", i, current->covers[i]);

}

void printTheMT(struct minTerm *current) {              // For Debug, DELETE IT AND DON"T FORGET TO FREE MEMORY.

    printf("\nThe Minterm (%d) :", current->value);
    printf("\nx = %d\n\n", current->max);
    for (int i = 0; i < current->max; i++)
        printf("coveredBy[%d] = %d\n", i, current->coveredBy[i]);

}

void buildpetricksMethod(char table[], int tableSize, int primeImplicantsArr[], int numOfPI, int n) {

    /*for(int j=0;j<tableSize;j++)
        printf("Table[%d] = %d\n",j,table[j]);
        */

    struct primeImp *primeImplicantsData = (struct primeImp *) malloc(numOfPI * sizeof(struct primeImp));
    for (int i = 0; i < numOfPI; i++) {
        int triArr[n];
        convertToTrinry(primeImplicantsArr[i], triArr, n);
        primeImplicantsData[i].pI = sumTrinaryArr(triArr, n);
        primeImplicantsData[i].n = countTwos(triArr, n);
        creatElementsInsidePrimeImp(primeImplicantsData + i, n);
        int k = 0;
        for (int j = 0; j < n; j++)
            if (triArr[j] == 2) {
                primeImplicantsData[i].twosIndex[k] = j;
                k++;
            }
        for (int h = 0; h < k; h++) {
            triArr[primeImplicantsData[i].twosIndex[h]] = 0;
        }

        primeImplicantsData[i].covers[0] = sumTrinaryArr(triArr, n);

        for (int g = 1; g < powerIt(2, k); g++) {

            triArr[primeImplicantsData[i].twosIndex[k - 1]]++;

            for (int j = k - 1; j >= 0; j--) {
                if (triArr[primeImplicantsData[i].twosIndex[j]] > 1) {
                    triArr[primeImplicantsData[i].twosIndex[j]] = 0;
                    triArr[primeImplicantsData[i].twosIndex[j - 1]]++;
                }
            }

            primeImplicantsData[i].covers[g] = sumTrinaryArr(triArr, n);

        }

        //printThePI(primeImplicantsData+i);
    }

    struct term *essentialPrimeImplicants = NULL;              //FREE IT

    int counter = 0;
    for (int i = 0; i < tableSize; i++)
        if (table[i] == 1)
            counter++;

    struct minTerm minTermsTable[counter];
    for (int i = 0; i < counter; i++) {
        for (int j = 0; j < tableSize; j++)                // Search for the next one
            if (table[j] == 1) {
                minTermsTable[i].value = decBinTri(j);
                minTermsTable[i].stillNeeded = 1;
                minTermsTable[i].indecator = 0;
                table[j] = 0;
                break;
            }
        int numOfCovers = 0;
        for (int j = 0; j < numOfPI; j++) {                 // Check How Many Covers it
            for (int k = 0; k < powerIt(2, primeImplicantsData[j].n); k++)
                if (primeImplicantsData[j].covers[k] == minTermsTable[i].value) {
                    numOfCovers++;
                    break;
                }
        }
        creatElementsInsideMinTerm(minTermsTable + i, numOfCovers);

        int h = 0;
        for (int j = 0; j < numOfPI; j++) {                 // Check Who Covers it
            for (int k = 0; k < powerIt(2, primeImplicantsData[j].n); k++)
                if (primeImplicantsData[j].covers[k] == minTermsTable[i].value) {
                    minTermsTable[i].coveredBy[h] = primeImplicantsData[j].pI;
                    h++;
                    break;
                }
        }
        if (numOfCovers == 1) {
            minTermsTable[i].stillNeeded = 0;
            if (essentialPrimeImplicants == NULL) {
                essentialPrimeImplicants = createTerm(
                        minTermsTable[i].coveredBy[0]);       // Create a term and store it initially in the array of pointers
            } else {
                struct term *newPtr = createTerm(minTermsTable[i].coveredBy[0]);
                struct term *end = essentialPrimeImplicants;
                int alreadyHere = 0;
                while (end->next != NULL) {
                    if (newPtr->value == end->value)
                        alreadyHere++;
                    end = end->next;
                }
                if (newPtr->value == end->value)
                    alreadyHere++;
                if (alreadyHere == 0)
                    end = append(end, newPtr);              // Add anther term at the end of the list
                else
                    free(newPtr);
            }
        }

        //printTheMT(minTermsTable+i);

    }

    struct term *ptr = essentialPrimeImplicants;
    int y = 0;
    while (ptr != NULL) {
        ptr = ptr->next;
        y++;
    }
    ptr = essentialPrimeImplicants;
    int g = 0;
    while (ptr != NULL) {
        if (y < 10)
            printf("\t\t\t     arrayOfEssentialPrimeImplicants[%d] = ", g);
        else if (y < 100)
            printf("\t\t\t     arrayOfEssentialPrimeImplicants[%.2d] = ", g);
        else if (y < 1000)
            printf("\t\t\t     arrayOfEssentialPrimeImplicants[%.3d] = ", g);
        else if (y < 10000)
            printf("\t\t\t     arrayOfEssentialPrimeImplicants[%.4d] = ", g);
        else if (y < 100000)
            printf("\t\t\t     arrayOfEssentialPrimeImplicants[%.5d] = ", g);
        else if (y < 1000000)
            printf("\t\t\t     arrayOfEssentialPrimeImplicants[%.6d] = ",
                   g); // More than 262144 is impossible because the cap is 19 variables
        int triArr[n];
        convertToTrinry(ptr->value, triArr, n);
        for (int j = n - 1; j >= 0; j--) {
            if (triArr[j] == 2)
                printf("-");
            else
                printf("%d", triArr[j]);
        }
        printf("\n");
        ptr = ptr->next;
        g++;
    }

    printf("\nPetrick's method time :\n\n");

    for (int i = 0; i < numOfPI; i++) {
        free(primeImplicantsData[i].twosIndex);
        free(primeImplicantsData[i].covers);
    }
    free(primeImplicantsData);

    applyPetrickMethod(minTermsTable, counter, essentialPrimeImplicants, y, n);

    freeRow(essentialPrimeImplicants);

    /*for(int j=0;j<tableSize;j++)
            printf("Table[%d] = %d\n",j,table[j]);*/

    //start combinations
    //if you found a cheaper

}

void creatElementsInsidePrimeImp(struct primeImp *current, int x) {
    current->twosIndex = (int *) malloc(x * sizeof(int));
    current->covers = (int *) malloc(powerIt(2, x) * sizeof(int));
}

int countTwos(int arr[], int x) {
    int counter = 0;
    for (int i = 0; i < x; i++)
        if (arr[i] == 2)
            counter++;
    return counter;
}

void creatElementsInsideMinTerm(struct minTerm *current, int n) {
    current->max = n;
    current->coveredBy = (int *) malloc(n * sizeof(int));
}

int calculateCombCost(int *, int, int, struct term *, int);

void
applyPetrickMethod(struct minTerm minTermsTable[], int counter, struct term *essentialPrimeImplicants, int y, int n) {

    // Note : only the essentially covered minterms are not stillneeded, you need to unneed the others who got covered in the way.

    if (essentialPrimeImplicants) {
        for (int i = 0; i < counter; i++) {
            if (minTermsTable[i].stillNeeded) {
                struct term *end = essentialPrimeImplicants;
                int alreadyHere = 0;
                for (int j = 0; j < minTermsTable[i].max; j++) {
                    while (end->next != NULL) {
                        if (minTermsTable[i].coveredBy[j] == end->value)
                            alreadyHere++;
                        end = end->next;
                    }
                    if (minTermsTable[i].coveredBy[j] == end->value)
                        alreadyHere++;
                    if (alreadyHere) {
                        minTermsTable[i].stillNeeded = 0;
                        //printf("The Minterm %d is no longer needed",minTermsTable[i].value);
                        break;
                    }
                }
            }
        }
    }

    int combArrNum = 0;
    for (int i = 0; i < counter; i++)
        if (minTermsTable[i].stillNeeded)
            combArrNum++;
        else
            free(minTermsTable[i].coveredBy);

    struct minTerm minTermsTableICareFor[combArrNum];

    int k = 0;
    for (int i = 0; i < counter; i++) {
        if (minTermsTable[i].stillNeeded) {
            minTermsTable[i].stillNeeded = 0;
            minTermsTableICareFor[k].value = minTermsTable[i].value;
            minTermsTableICareFor[k].indecator = 0;
            creatElementsInsideMinTerm(minTermsTableICareFor + k, minTermsTable[i].max);
            for (int j = 0; j < minTermsTable[i].max; j++)
                minTermsTableICareFor[k].coveredBy[j] = minTermsTable[i].coveredBy[j];
            free(minTermsTable[i].coveredBy);
            //printTheMT(minTermsTableICareFor+k);
            k++;
        }
    }       //indicator = 0,max defined, coveredBy as well all is ready sir.

    int cheapestCombination[k];
    int leastCost;

    if (combArrNum) {

        //Start the combinations
        int combination[k];


        if (k) {
            for (int i = 0; i < k; i++) {
                combination[i] = minTermsTableICareFor[i].coveredBy[minTermsTableICareFor[i].indecator];
                cheapestCombination[i] = combination[i];
            }
            leastCost = calculateCombCost(combination, k, n, essentialPrimeImplicants, y);
        }


        while ((minTermsTableICareFor[0].indecator < minTermsTableICareFor[0].max)) {

            minTermsTableICareFor[k - 1].indecator++;

            for (int j = k - 1; j > 0; j--) {
                if (minTermsTableICareFor[j].indecator >= minTermsTableICareFor[j].max) {
                    minTermsTableICareFor[j].indecator = 0;
                    minTermsTableICareFor[j - 1].indecator++;
                }
            }

            int currentCost;

            if (k) {
                for (int i = 0; i < k; i++)
                    combination[i] = minTermsTableICareFor[i].coveredBy[minTermsTableICareFor[i].indecator];
            }
            currentCost = calculateCombCost(combination, k, n, essentialPrimeImplicants, y);

            printf("\t\t\t     F = ");
            for (int g = 0; g < k; g++) {
                int alreadyHere = 0;
                for (int j = g + 1; j < combArrNum; j++) {
                    if (cheapestCombination[g] == cheapestCombination[j])
                        alreadyHere++;
                    break;
                }
                if (alreadyHere)
                    continue;
                int triArr[n];
                convertToTrinry(cheapestCombination[g], triArr, n);
                for (int j = n - 1; j >= 0; j--) {
                    if (triArr[j] == 2)
                        printf("-");
                    else
                        printf("%d", triArr[j]);
                }
                printf(" + ");
            }
            struct term *end = essentialPrimeImplicants;
            while (end != NULL) {
                int triArr[n];
                convertToTrinry(end->value, triArr, n);
                for (int j = n - 1; j >= 0; j--) {
                    if (triArr[j] == 2)
                        printf("-");
                    else
                        printf("%d", triArr[j]);
                }
                if (end->next != NULL)
                    printf(" + ");
                end = end->next;
                if (end == NULL)
                    break;
            }
            puts("");

            if (currentCost <= leastCost) {
                for (int i = 0; i < k; i++)
                    cheapestCombination[i] = combination[i];
                leastCost = currentCost;
            }

        }
    }

    printf("least price formula is : \n");
    printf("\t\t\t     F = ");
    for (int g = 0; g < k; g++) {
        int alreadyHere = 0;
        for (int j = g + 1; j < combArrNum; j++) {
            if (cheapestCombination[g] == cheapestCombination[j])
                alreadyHere++;
            break;
        }
        if (alreadyHere)
            continue;
        int triArr[n];
        convertToTrinry(cheapestCombination[g], triArr, n);
        for (int j = n - 1; j >= 0; j--) {
            if (triArr[j] == 2)
                printf("-");
            else
                printf("%d", triArr[j]);
        }
        printf(" + ");
    }

    struct term *end = essentialPrimeImplicants;
    while (end != NULL) {
        int triArr[n];
        convertToTrinry(end->value, triArr, n);
        for (int j = n - 1; j >= 0; j--) {
            if (triArr[j] == 2)
                printf("-");
            else
                printf("%d", triArr[j]);
        }
        if (end->next != NULL)
            printf(" + ");
        end = end->next;
        if (end == NULL)
            break;
    }

}

int calculateCombCost(int combination[], int combArrNum, int n, struct term *essentialPrimeImplicants, int y) {

    int totalCost = 0;

    int notCosts[n];

    for (int i = 0; i < n; i++)
        notCosts[n] = 0;

    struct term *end = essentialPrimeImplicants;
    while (end != NULL) {
        int triArr[n];
        convertToTrinry(end->value, triArr, n);
        for (int j = 0; j < n; j++)
            if (triArr[j] != 2) {
                totalCost++;
                if (triArr[j] == 0)
                    notCosts[j] = 1;
            }
        end = end->next;
        if (end == NULL)
            break;
    }

    for (int g = 0; g < combArrNum; g++) {
        int alreadyHere = 0;
        for (int j = g + 1; j < combArrNum; j++) {
            if (combination[g] == combination[j])
                alreadyHere++;
            break;
        }
        if (alreadyHere)
            continue;
        int triArr[n];
        convertToTrinry(combination[g], triArr, n);
        for (int j = 0; j < n; j++)
            if (triArr[j] != 2) {
                totalCost++;
                if (triArr[j] == 0)
                    notCosts[j] = 1;
            }
    }

    for (int i = 0; i < n; i++)
        if (notCosts[i])
            totalCost++;

    return totalCost;
}
